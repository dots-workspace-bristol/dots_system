#!/usr/bin/env python3

import math
import operator
import random
import struct
import time

import numpy as np

import geometry_msgs.msg
import nav_msgs.msg
import sensor_msgs.msg
import std_msgs.msg

import py_trees
import py_trees_ros

import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data, qos_profile_system_default

import tf2_ros

import dots_interfaces.msg


class G:
    # Global values
    max_linear_velocity = 0.5
    max_angular_velocity = 3.0

    # P controllers
    Pv = 2.0
    Pw = 2.0
    carriers = set(range(100, 110))
    camera_angles = [i * np.pi/2 + 4 for i in range(4)] # [0.2967, 2.251, -2.251, -0.2967]

    max_under_velocity      = 0.3
    max_carry_velocity      = 0.3

def rpy_from_quaternion(quaternion):
    x = quaternion.x
    y = quaternion.y
    z = quaternion.z
    w = quaternion.w

    sinr_cosp = 2 * (w * x + y * z)
    cosr_cosp = 1 - 2 * (x * x + y * y)
    roll = np.arctan2(sinr_cosp, cosr_cosp)

    sinp = 2 * (w * y - z * x)
    pitch = np.arcsin(sinp)

    siny_cosp = 2 * (w * z + x * y)
    cosy_cosp = 1 - 2 * (y * y + z * z)
    yaw = np.arctan2(siny_cosp, cosy_cosp)
    return roll, pitch, yaw

def quaternion_from_rpy(roll, pitch, yaw):
    cy = math.cos(yaw * 0.5)
    sy = math.sin(yaw * 0.5)
    cp = math.cos(pitch * 0.5)
    sp = math.sin(pitch * 0.5)
    cr = math.cos(roll * 0.5)
    sr = math.sin(roll * 0.5)

    q = [0] * 4
    q[0] = sr * cp * cy - cr * sp * sy
    q[1] = cr * sp * cy + sr * cp * sy
    q[2] = cr * cp * sy - sr * sp * cy
    q[3] = cr * cp * cy + sr * sp * sy
    return q

class Square:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.in_dropzone = False

    def set_dropzone(self):
        self.in_dropzone = True


class Controller(Node):
    def __init__(self):
        # Create the controller node
        super().__init__('controller')

        # Publish to one topic, the command velocity
        self.cmd_vel_pub        = self.create_publisher(geometry_msgs.msg.Twist, 'cmd_vel', 10)

        self.setup_subscriptions()
        self.lifter_pub = self.create_publisher(std_msgs.msg.Bool, 'lifter', 10)

        self.command            = geometry_msgs.msg.Twist()
        self.heading            = random.uniform(0, math.pi)

        self.max_range          = 2.0
        self.min_range          = 0.13
        self.collision_range    = 0.3
        self.speed              = 0.5
        # Start off with forward motion
        self.command.linear.x   = self.speed

        # Start a timer to run the control loop at 10Hz
        self.timer              = self.create_timer(0.1, self.control_loop_callback)

        # random wander
        self.random_cmd_vel = None
        self.collision = None
        self.angle = None

        # irtof
        self.obstacle = None
        self.collision = None
        self.avoid_cmd_vel = None
        self.was_collision = None
        self.seen_carrier = None

        self.max_range = 2.0
        self.min_range = 0.13
        self.collision_range = 0.3
        self.speed = 0.5
        self.irtof = None

        # vision
        self.pause = False
        self.align_cmd_vel = None
        self.centre_cmd_vel = None
        self.robot_name = self.get_namespace()[1:]

        # check tranforms between frames
        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer, self)

        # target id?
        self.target = None
        self.seen_carrier = False
        self.under_carrier = False
        self.centered_carrier = False
        self.ticks_since_not_seen = 0
        self.ticks_centering = 0
        self.align_transform = None
        self.centre_transform = None
        ###

        # compass
        self.away_cmd_vel = None
        self.home_cmd_vel = None
        self.in_dropzone = None
        self.carrying = False
        self.was_carrying = False
        self.escape_cmd = None

        #
        self.position_list = []
        self.squares = {}
        self.current_square = None

        #
        self.dropped_boxes_pos = []

        self.litte_ticks = 0

        self.current_dists = []

        self.level = False
        self.heading = None


    def setup_subscriptions(self):
        # irtof sub
        self.irtof_sub = self.create_subscription(
            sensor_msgs.msg.PointCloud2,
            'sensor/scan',
            self.scan_callback,
            qos_profile=qos_profile_sensor_data
        )

        # set up camera subscriptions
        cam_num = 5
        self.cam_tags_subs = []
        for i in range(cam_num):
            self.cam_tags_subs.append(self.create_subscription(
                dots_interfaces.msg.TagArray,
                f'cam{i}_tags',
                lambda msg, i=i: self.cam_tags_callback(msg, i),
                qos_profile_system_default
            ))
        
        # compas sub
        self.compass_sub = self.create_subscription(
            std_msgs.msg.Float32,
            'sensor/compass',
            self.compass_callback,
            qos_profile_sensor_data
        )

        # drop zone sub
        self.dropzone_sub = self.create_subscription(
            std_msgs.msg.Bool,
            'sensor/dropzone',
            self.dropzone_callback,
            qos_profile_sensor_data
        )

        # odom
        self.odom_sub = self.create_subscription(
            nav_msgs.msg.Odometry,
            'odom',
            self.odom_callback,
            qos_profile_sensor_data
        )
    
    def odom_callback(self, msg):
        # self.get_logger().info(f'{msg}')
        p = msg.pose.pose.position
        self.position_list.append((p.x, p.y))

        x = int(round(p.x, 1) * 10)
        y = int(round(p.y, 1) * 10)

        pos = (x, y)

        if pos in self.squares:
            pass
        else:
            self.squares[pos] = Square(*pos)
            self.current_square = self.squares[pos]

        #self.get_logger().info(f'{self.position_list}')

    def scan_callback(self, msg):
        self.irtof = msg

    def cam_tags_callback(self, msg, cam):
        if cam in range(4):
            self.side_cam(msg, cam)
        elif cam == 4:
            self.up_cam(msg)
        else:
            raise ValueError('Invalid cam number')
    
    def side_cam(self, msg, cam):
        if self.seen_carrier and (self.docking_cam != cam):
            return
        if self.seen_carrier:
            # self.get_logger().info(f'Callback cam {cam}')
            pass

        cam_prefix = f'{self.robot_name}_cam{cam}_'
        transforms = []
        for tag in msg.data:
            if tag.id in G.carriers:
                # self.get_logger().info(f'i can see {tag.id}')
                try:
                    transform = self.tfBuffer.lookup_transform(
                        f'{cam_prefix}link', 
                        f'{cam_prefix}fid{tag.id}',
                        self.get_clock().now() - rclpy.duration.Duration(seconds=0.1))
                except (tf2_ros.LookupException, 
                        tf2_ros.ConnectivityException, 
                        tf2_ros.ExtrapolationException) as e:
                    self.get_logger().info(f'Failed tf lookup {e}')
                else:
                    # self.node.get_logger().info('Tr %s' % transform)
                    transforms.append((tag, transform))

        if transforms:
            self.get_logger().info(f'Update transforms')
            if self.seen_carrier:
                ids = [t[0].id for t in transforms]
                if self.align_transform[0].id in ids:
                    transform = next(t for t in transforms if t[0].id == self.align_transform[0].id)
                else:
                    transform = transforms[0]
                    self.get_logger().info('lost a tag')
            else:
                transform = transforms[0]

            # self.get_logger().info(f'{transform}')
            self.align_transform = transform
            self.seen_carrier = True
            self.docking_cam = cam
            self.ticks_since_not_seen = 0
    
    def up_cam(self, msg):
        if self.was_carrying or self.in_dropzone:
            return
        if msg.data:
            self.get_logger().info(f'i am under {msg.data[0].id}')
            try:
                self.centre_transform = self.tfBuffer.lookup_transform(
                    f'{self.robot_name}_base_link', 
                    f'{self.robot_name}_cam4_fid{msg.data[0].id}',
                    self.get_clock().now() - rclpy.duration.Duration(seconds=0.4))
            except (tf2_ros.LookupException, 
                    tf2_ros.ConnectivityException, 
                    tf2_ros.ExtrapolationException) as e:
                self.get_logger().info(f'Failed tf lookup {e}')
            else:
                self.under_carrier = True
                self.under_tag = msg.data[0].id
                self.ticks_since_not_seen = 0
    
    def compass_callback(self, msg):
        heading = msg.data
        self.heading = heading
        hcv = geometry_msgs.msg.Twist()
        hcv.linear.x = G.max_carry_velocity * np.cos(-heading)
        hcv.linear.y = G.max_carry_velocity * np.sin(-heading)
        self.home_cmd_vel = hcv

        acv = geometry_msgs.msg.Twist()
        acv.linear.x = G.max_linear_velocity * np.cos(np.pi - heading)
        acv.linear.y = G.max_linear_velocity * np.sin(np.pi - heading)
        self.away_cmd_vel = acv
    
    def dropzone_callback(self, msg):
        self.in_dropzone = msg.data
        if self.current_square is not None:
            self.current_square.set_dropzone()
    
    def reset(self):
        self.seen_carrier = False
        self.under_carrier = False
        self.centered_carrier = False
        self.ticks_centering = 0
        self.align_transform = None

    def process_vision(self):
        if self.was_carrying or self.in_dropzone:
            return
        self.ticks_since_not_seen += 1
        if self.ticks_since_not_seen > 10:
            self.seen_carrier = False
            self.under_carrier = False
            self.centered_carrier = False
            self.ticks_centering = 0
            if self.align_transform is not None:
                self.get_logger().info('missing in action')
                self.align_transform = None
        elif self.under_carrier:
            self.yes_id = self.align_transform[0].id
            self.ticks_centering += 1
            if self.ticks_centering > 10:
                self.centered_carrier = True
            tag = self.align_transform[0]
            self.get_logger().info(f'party time, im focused on {self.under_tag}')
            tr = self.centre_transform.transform.translation
            msg = geometry_msgs.msg.Twist()
            msg.linear.x = np.clip(tr.x, -0.3, 0.3)
            msg.linear.y = np.clip(tr.y, -0.3, 0.3)
            self.centre_cmd_vel = msg
        elif self.seen_carrier:
            tag = self.align_transform[0]
            self.get_logger().info(f'boogie time, im focused on {tag.id}')
            tr = self.align_transform[1].transform.translation

            # move alg
            msg = geometry_msgs.msg.Twist()
            th = G.camera_angles[self.docking_cam]# - np.pi #+ self.heading
            speed = 0.3
            msg.linear.x = tr.x * np.cos(th)
            msg.linear.y = tr.y * np.sin(th)
            #msg.linear.x = math.sqrt(speed) * np.sin(th)
            #msg.linear.y = math.sqrt(speed) * np.cos(th)
            ## msg.angular.z = 2 * math.atan2(tr.y, tr.x)
            ## msg.linear.x = 1 * math.sqrt(tr.x ** 2 + tr.y ** 2)

            ## 
            r,p,y   = rpy_from_quaternion(self.align_transform[1].transform.rotation)
            #msg.linear.x    = G.Pv * (np.cos(th) * tr.x - np.sin(th) * tr.y)
            #msg.linear.y    = G.Pv * (np.sin(th) * tr.x + np.cos(th) * tr.y)
            #msg.angular.z   = G.Pw * y
            self.get_logger().info(f'{r} {p} {y} {th} {self.heading} {tr.x} {tr.y}')

            self.align_cmd_vel = msg
            #if self.rotate_to(th):
            #    self.get_logger().info('lined up!')
            #    msg.linear.x = 2.0
            #    self.cmd_vel_pub.publish(msg)
        else:
            self.get_logger().info('im boring')
    
    def move_platform(self, lift=True):
        for i in range(10):
            msg = std_msgs.msg.Bool()
            msg.data = lift
            self.lifter_pub.publish(msg)
        time.sleep(0.5)
        self.carrying = lift

    def process_obstacles(self):
        if self.irtof is None:
            return True

        if self.seen_carrier:
            self.get_logger().info('i should not be here')

        # There are 16 sensors, make somewhere to store data from each one. The Gazebo
        # version of the laser scanner doesn't send data for points with no return detected
        # but the hardware does, so fill in the maximum range for each entry to start with.
        msg = self.irtof
        data        = np.zeros((16))
        data[:]     = self.max_range
        step        = math.pi / 8.0
        prox        = np.zeros(2)
        collision   = False
        for i in range(msg.width):
            # Points within the pointcloud are actually locations in 3D space in the scanner
            # frame. Each is a float32 taking 12 bytes. 
            # Extract point
            [x, y, z]   = struct.unpack('fff', bytes(msg.data[i * msg.point_step : i * msg.point_step + 12]))
            # Get angle and see which sensor it was
            th          = math.atan2(y, x)
            if th < 0.0:
                th += 2 * math.pi
            idx         = int(round(th / step))
            # Get the distance and save it in the array
            dist        = math.sqrt(x**2 + y**2)
            data[idx]   = dist
            # Check if there is a collision
            if dist < self.collision_range:
                collision = True
            # self.get_logger().info(f'{dist}')
            # Calculate a vector pointing towards the nearest obstacle
            nm          = np.array([x, y]) / dist
            nm_inv_len  = 1 - (dist - self.min_range) / (self.max_range - self.min_range)
            nm          = nm * nm_inv_len
            prox        += nm 

        self.obstacle    = prox
        self.collision   = collision
        if collision:
            self.was_collision = True

        cv = geometry_msgs.msg.Twist()

        coll_vector     = -prox / np.linalg.norm(prox)
        cv.linear.x     = coll_vector[0] * G.max_linear_velocity
        cv.linear.y     = coll_vector[1] * G.max_linear_velocity
        cv.angular.z    = random.uniform(-G.max_angular_velocity / 2.0, G.max_angular_velocity / 2.0)

        self.avoid_cmd_vel = cv

        # self.node.get_logger().info('%s' % irtof)
        if collision:
            self.get_logger().info('Collision!')
        self.irtof = None
    
    def process_micro_ob(self):
        if self.irtof is None:
            return True

        msg = self.irtof
        data = np.zeros((16))
        data[:] = self.max_range
        step = math.pi / 8.0
        prox  = np.zeros(2)
        collision = False
        for i in range(msg.width):
            # Extract point
            [x, y, z] = struct.unpack('fff', bytes(msg.data[i * msg.point_step : i * msg.point_step + 12]))
            # Get angle and see which sensor it was
            th = math.atan2(y, x)
            if th < 0.0:
                th += 2 * math.pi
            idx = int(round(th / step))
            # Get the distance and save it in the array
            dist  = math.sqrt(x**2 + y**2)
        self.get_logger().info(f'{data}')
        self.current_dists = data

        start = 0
        end = 0
        best = (0, 0)
        for index, dist in enumerate(data):
            if dist < 1.9:
                start = index
            else:
                end = index
                if end - start > best[1] - best[0]:
                    best = (start, end)
        
        self.get_logger().info(f'{best}')
        direction = math.ceil(float(best[0] + best[1]) / 2)
        self.get_logger().info(f'{direction}')

        cv = geometry_msgs.msg.Twist()
        cv.linear.x     = 0.3
        cv.linear.y     = 0.3
        cv.angular.z    = 2 * np.pi * direction/16
        self.escape_cmd = cv

    def random_wander(self):
        if self.collision:
            self.get_logger().info('there is a collision so should be no movement')

        if self.angle is None or self.was_collision:
            self.get_logger().info('change of direction!')
            angle = random.uniform(-math.pi, math.pi)
            self.angle = angle
            self.was_collision = False

        angle = self.angle
        # self.get_logger().info('keep on swimming')

        cv = geometry_msgs.msg.Twist()
        cv.linear.x = G.max_linear_velocity * math.cos(angle)
        cv.linear.y = G.max_linear_velocity * math.sin(angle)
        cv.angular.z = 0.0
        self.random_cmd_vel = cv
    
    def do_nothing(self):
        self.cmd_vel_pub.publish(geometry_msgs.msg.Twist())
    
    def fix_rotate(self):
        while self.process_micro_ob():
            pass
        away_z = self.away_cmd_vel.angular.z
        escape_z = self.escape_cmd.angular.z
        self.get_logger().info(f'Away {away_z}, escape {escape_z}')

        cv = geometry_msgs.msg.Twist()
        cv.angular.z = 3.0

        self.rotate_cmd = cv

        return 3.1 < abs(escape_z) < 3.7 or all(d > 1.9 for d in self.current_dists)
    
    def little_push(self):
        self.litte_ticks += 1
        if self.litte_ticks > 15:
            self.push_2 += 1
            if self.push_2 > 15:
                self.litte_ticks = 0
                return True
            else:
                cv = geometry_msgs.msg.Twist()
                cv.linear.x = 0.7 * np.sin(self.heading)
                cv.linear.y = 0.7 * np.cos(self.heading)
                self.get_logger().info(f'Little 2 pushing')
                self.cmd_vel_pub.publish(cv)
        else:
            cv = geometry_msgs.msg.Twist()
            cv.linear.x = 0.7 * self.home_cmd_vel.linear.x
            cv.linear.y = 0.7 * self.home_cmd_vel.linear.y
            self.get_logger().info(f'Little pushing')
            self.cmd_vel_pub.publish(cv)
            self.push_2 = 0
        return False
        
    
    def drop_box(self):
        if self.fix_rotate():
            if self.little_push():
                self.move_platform(lift=False)
                #G.carriers.remove(self.yes_id)
                #self.get_logger().info(f'Removing {self.yes_id}')
                self.get_logger().info(f'{G.carriers}')
                self.reset()
                self.was_carrying = True

                mid_x = self.current_square.x
                mid_y = self.current_square.y

                delta = 6
                for x in range(mid_x - delta, mid_x + delta):
                    for y in range(mid_y - delta, mid_y + delta):
                        pos = (x, y)
                        if pos not in self.squares:
                            self.squares[pos] = Square(x, y)
                        square = self.squares[pos]
                        self.dropped_boxes_pos.append(self.current_square)
        else:
            self.cmd_vel_pub.publish(self.rotate_cmd)
    
    def near_bad_drop(self):
        mid_x = self.current_square.x
        mid_y = self.current_square.y

        delta = 5
        for x in range(mid_x - delta, mid_x + delta):
            for y in range(mid_y - delta, mid_y + delta):
                pos = (x, y)
                if pos in self.squares and self.squares[pos] in self.dropped_boxes_pos:
                    return True
        return False

    def rotate_to(self, angle=0):
        delta = angle - self.heading
        self.get_logger().info(f'Delta: {delta}')
        if abs(delta) > 0.2:
            self.get_logger().info('lining up...')
            # correction
            command = geometry_msgs.msg.Twist()
            multiplier = 1 if delta > 0 else -1
            speed = 3.0 if delta > 0.3 else 1.0
            command.angular.z = multiplier * speed
            self.get_logger().info(f'rot {command.angular.z}')
            self.cmd_vel_pub.publish(command)
            return False
        else:
            return True
    
    def fix_level(self):
        if abs(self.heading) > 0.1:
            command = geometry_msgs.msg.Twist()
            multiplier = 1 if self.heading < 0 else -1
            command.angular.z = multiplier * 3.0
            self.cmd_vel_pub.publish(command)
            return False
        else:
            return True

    def control_loop_callback(self):
        self.process_vision()
        if self.carrying:
            if self.near_bad_drop():
                if self.rotate_to(0):
                    command = geometry_msgs.msg.Twist()
                    command.linear.y = -1.0
                    self.cmd_vel_pub.publish(command)
            elif self.in_dropzone and self.carrying:
                self.drop_box()
            else:
                self.cmd_vel_pub.publish(self.home_cmd_vel)
        elif self.current_square in self.dropped_boxes_pos:
            self.cmd_vel_pub.publish(self.away_cmd_vel)
        elif self.in_dropzone and self.was_carrying:
            self.cmd_vel_pub.publish(self.away_cmd_vel)
            # self.cmd_vel_pub.publish(self.escape_cmd)
        elif self.was_carrying:
            self.cmd_vel_pub.publish(self.away_cmd_vel)
            time.sleep(1)
            self.was_carrying = False
        elif self.centered_carrier and not self.in_dropzone:
            self.move_platform(lift=True)
        elif self.under_carrier and not self.in_dropzone:
            self.cmd_vel_pub.publish(self.centre_cmd_vel)
        elif self.seen_carrier:
            if self.in_dropzone:
                tag = self.align_transform[0]
                #if tag.id in G.carriers:
                #    G.carriers.remove(tag.id)
                #    self.get_logger().info(f'Removing {tag.id}')
                #    self.get_logger().info(f'{G.carriers}')
                self.reset()
            else:
                self.cmd_vel_pub.publish(self.align_cmd_vel)
                #pass
        elif self.process_obstacles():
            self.do_nothing()  # waiting
        elif self.collision:
            self.cmd_vel_pub.publish(self.avoid_cmd_vel)
        else:
            self.random_wander()
            self.cmd_vel_pub.publish(self.random_cmd_vel)


def main():
    rclpy.init()
    controller = Controller()
    try:
        rclpy.spin(controller)
    except:
        controller.do_nothing()
        raise


if __name__ == '__main__':
    main()
