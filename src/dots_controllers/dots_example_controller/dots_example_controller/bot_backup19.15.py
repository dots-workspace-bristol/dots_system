#!/usr/bin/env python3

import math
import operator
import random
import struct

import numpy as np

import geometry_msgs.msg
import sensor_msgs.msg

import py_trees
import py_trees_ros

import rclpy
from rclpy.qos import qos_profile_sensor_data, qos_profile_system_default

import tf2_ros

import dots_interfaces.msg


class G:
    # Global values
    max_linear_velocity = 0.5
    max_angular_velocity = 3.0

    # P controllers
    Pv = 2.0
    Pw = 2.0
    carriers = set(range(100, 110))
    camera_angles = [0.2967, 2.251, -2.251, -0.2967]

def rpy_from_quaternion(quaternion):
    x = quaternion.x
    y = quaternion.y
    z = quaternion.z
    w = quaternion.w

    sinr_cosp = 2 * (w * x + y * z)
    cosr_cosp = 1 - 2 * (x * x + y * y)
    roll = np.arctan2(sinr_cosp, cosr_cosp)

    sinp = 2 * (w * y - z * x)
    pitch = np.arcsin(sinp)

    siny_cosp = 2 * (w * z + x * y)
    cosy_cosp = 1 - 2 * (y * y + z * z)
    yaw = np.arctan2(siny_cosp, cosy_cosp)
    return roll, pitch, yaw

def quaternion_from_rpy(roll, pitch, yaw):
    cy = math.cos(yaw * 0.5)
    sy = math.sin(yaw * 0.5)
    cp = math.cos(pitch * 0.5)
    sp = math.sin(pitch * 0.5)
    cr = math.cos(roll * 0.5)
    sr = math.sin(roll * 0.5)

    q = [0] * 4
    q[0] = sr * cp * cy - cr * sp * sy
    q[1] = cr * sp * cy + sr * cp * sy
    q[2] = cr * cp * sy - sr * sp * cy
    q[3] = cr * cp * cy + sr * sp * sy
    return q


class Pick_random_direction(py_trees.behaviour.Behaviour):
    def __init__(self, name):
        super(Pick_random_direction, self).__init__(name)
        self.blackboard = self.attach_blackboard_client(name)
        self.blackboard.register_key(
            key='random_cmd_vel',
            access=py_trees.common.Access.WRITE)
        self.blackboard.register_key(
            key='collision',
            access=py_trees.common.Access.READ,
            required=True)
        self.blackboard.register_key(
            key='angle',
            access=py_trees.common.Access.WRITE)
        self.blackboard.register_key(
            key='was_collision',
            access=py_trees.common.Access.WRITE,
            required=True)

    
    def setup(self, **kwargs):
        try:
            self.node = kwargs['node']
        except KeyError as e:
            self.node = None
            print(e)
            raise
        
        self.blackboard.angle = None

    def update(self):
        bb_angle = self.blackboard.angle

        if self.blackboard.collision:
            self.logger.info('there is a collision so no movement')

        if bb_angle is None or self.blackboard.was_collision:
            self.logger.info('change of direction!')
            angle = random.uniform(-math.pi, math.pi)
            self.blackboard.angle = angle
            self.blackboard.was_collision = False
        else:
            angle = bb_angle
            # self.logger.info('keep on swimming')
        
        cv = geometry_msgs.msg.Twist()
        cv.linear.x = G.max_linear_velocity * math.cos(angle)
        cv.linear.y = G.max_linear_velocity * math.sin(angle)
        cv.angular.z = 0.0
        self.blackboard.random_cmd_vel = cv
        return py_trees.common.Status.SUCCESS


class Process_irtof(py_trees.behaviour.Behaviour):
    def __init__(self, name):
        super(Process_irtof, self).__init__(name)
        self.blackboard = self.attach_blackboard_client(name)
        self.blackboard.register_key(key='irtof', access=py_trees.common.Access.WRITE)
        self.blackboard.register_key(key='obstacle', access=py_trees.common.Access.WRITE)
        self.blackboard.register_key(key='collision', access=py_trees.common.Access.WRITE)
        self.blackboard.register_key(key='avoid_cmd_vel', access=py_trees.common.Access.WRITE)
        self.blackboard.register_key(key='was_collision', access=py_trees.common.Access.WRITE)

        self.blackboard.register_key(key='seen_carrier', access=py_trees.common.Access.READ, required=True)

    def setup(self, **kwargs):
        try:
            self.node = kwargs['node']
        except KeyError as e:
            self.node = None
            print(e)
            raise

        self.blackboard.was_collision = False

        self.max_range          = 2.0
        self.min_range          = 0.13
        self.collision_range    = 0.3
        self.speed              = 0.5
        self.irtof_sub          = self.node.create_subscription(
            sensor_msgs.msg.PointCloud2,
            'sensor/scan',
            self.scan_callback,
            qos_profile=qos_profile_sensor_data
        )
        self.irtof = None
    
    def scan_callback(self, msg):
        self.irtof = msg


    def update(self):
        # Return running if the blackboard entry does not yet exist
        # self.logger.info(f'pre')
        if self.irtof is None:
            return py_trees.common.Status.RUNNING
        # self.logger.info(f'post')

        if self.blackboard.seen_carrier:
            self.logger.info('i should not be here')

        # There are 16 sensors, make somewhere to store data from each one. The Gazebo
        # version of the laser scanner doesn't send data for points with no return detected
        # but the hardware does, so fill in the maximum range for each entry to start with.
        msg = self.irtof
        data        = np.zeros((16))
        data[:]     = self.max_range
        step        = math.pi / 8.0
        prox        = np.zeros(2)
        collision   = False
        for i in range(msg.width):
            # Points within the pointcloud are actually locations in 3D space in the scanner
            # frame. Each is a float32 taking 12 bytes. 
            # Extract point
            [x, y, z]   = struct.unpack('fff', bytes(msg.data[i * msg.point_step : i * msg.point_step + 12]))
            # Get angle and see which sensor it was
            th          = math.atan2(y, x)
            if th < 0.0:
                th += 2 * math.pi
            idx         = int(round(th / step))
            # Get the distance and save it in the array
            dist        = math.sqrt(x**2 + y**2)
            data[idx]   = dist
            # Check if there is a collision
            if dist < self.collision_range:
                collision = True
            # self.logger.info(f'{dist}')
            # Calculate a vector pointing towards the nearest obstacle
            nm          = np.array([x, y]) / dist
            nm_inv_len  = 1 - (dist - self.min_range) / (self.max_range - self.min_range)
            nm          = nm * nm_inv_len
            prox        += nm 

        self.blackboard.obstacle    = prox
        self.blackboard.collision   = collision
        if collision:
            self.blackboard.was_collision = True

        cv = geometry_msgs.msg.Twist()

        coll_vector     = -prox / np.linalg.norm(prox)
        cv.linear.x     = coll_vector[0] * G.max_linear_velocity
        cv.linear.y     = coll_vector[1] * G.max_linear_velocity
        cv.angular.z    = random.uniform(-G.max_angular_velocity / 2.0, G.max_angular_velocity / 2.0)

        self.blackboard.avoid_cmd_vel = cv
        

        #self.node.get_logger().info('%s' % irtof)
        if collision:
            self.logger.info('Collision!')
        self.irtof = None
        return py_trees.common.Status.SUCCESS


class Process_vision(py_trees.behaviour.Behaviour):
    def __init__(self, name):
        super(Process_vision, self).__init__(name)
        self.blackboard = self.attach_blackboard_client(name)
        self.blackboard.register_key(key='visible_tags', access=py_trees.common.Access.WRITE)
        self.blackboard.register_key(key='seen_carrier', access=py_trees.common.Access.WRITE)
        self.blackboard.register_key(key='align_cmd_vel', access=py_trees.common.Access.WRITE)
    
    def setup(self, **kwargs):
        try:
            self.node = kwargs['node']
        except KeyError as e:
            self.node = None
            print('Should have node! %s' % e)
            sys.exit(1)

        # The robot name is always used for the namespace, get the name by removing the 
        # leading '/'
        self.robot_name = self.node.get_namespace()[1:]
        

        cam_num = 5
        self.blackboard.visible_tags = {i:[] for i in range(cam_num)}
        
        # set up camera subscriptions
        self.cam_tags_subs = []
        for i in range(cam_num):
            self.cam_tags_subs.append(self.node.create_subscription(
                dots_interfaces.msg.TagArray,
                f'cam{i}_tags',
                lambda msg, i=i: self.cam_tags_callback(msg, i),
                qos_profile_system_default
            ))

        # check tranforms between frames
        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer, self.node)

        # target id?
        self.target = None
        self.blackboard.seen_carrier = False
        self.ticks_since_not_seen = 1
        self.align_transform = None
        
    def cam_tags_callback(self, msg, cam):
        if self.blackboard.seen_carrier and (self.docking_cam != cam):
            return
        visible_tags = []  # self.blackboard.visible_tags[cam]

        cam_prefix = f'{self.robot_name}_cam{cam}_'
        transforms = []
        for tag in msg.data:
            if tag.id in G.carriers:
                visible_tags.append(tag)

                try:
                    transform = self.tfBuffer.lookup_transform(
                        f'{cam_prefix}link', 
                        f'{cam_prefix}fid{tag.id}',
                        self.node.get_clock().now(),
                        rclpy.duration.Duration(seconds=0.1))
                except (tf2_ros.LookupException, 
                        tf2_ros.ConnectivityException, 
                        tf2_ros.ExtrapolationException) as e:
                    self.logger.debug(f'Failed tf lookup {e}')
                else:
                    #self.node.get_logger().info('Tr %s' % transform)
                    transforms.append((tag, transform))
        

        

        if transforms:
            if self.blackboard.seen_carrier:
                ids = [t[0].id for t in transforms]
                if self.align_transform[0].id in ids:
                    transform = next(t for t in transforms if t[0].id == self.align_transform[0].id)
                else:
                    transform = transforms[0]
                    self.logger.info('lost a tag')
            else:
                transform = transforms[0]

            # self.logger.info(f'{transform}')
            self.align_transform = transform
            self.blackboard.seen_carrier = True
            self.docking_cam = cam
            self.ticks_since_not_seen = 0
        
        self.blackboard.visible_tags[cam] = visible_tags
        if visible_tags:
            pass  # self.logger.info(f'Tag update {cam}')
    
    def update(self):
        self.ticks_since_not_seen += 1
        if self.ticks_since_not_seen > 10:
            self.blackboard.seen_carrier = False
            if self.align_transform != None:
                self.logger.info('missing in action')
                self.align_transform = None
        elif self.blackboard.seen_carrier:
            tag = self.align_transform[0]
            self.logger.info(f'boogie time, i see {tag.id}')
            tr = self.align_transform[1].transform.translation
            
            # move alg
            msg = geometry_msgs.msg.Twist()
            ##msg.angular.z = 2 * math.atan2(tr.y, tr.x)
            ##msg.linear.x = 1 * math.sqrt(tr.x ** 2 + tr.y ** 2)


            ##th = G.camera_angles[self.docking_cam]
            ##r,p,y   = rpy_from_quaternion(self.align_transform[1].transform.rotation)
            ##msg.linear.x    = G.Pv * (np.cos(th) * tr.x - np.sin(th) * tr.y)
            ##msg.linear.y    = G.Pv * (np.sin(th) * tr.x + np.cos(th) * tr.y)
            ##msg.angular.z   = G.Pw * y

            self.blackboard.align_cmd_vel = msg
        else:
            self.logger.info('im boring')
            
        return py_trees.common.Status.SUCCESS
        

def create_root():
    root = py_trees.composites.Parallel(
        name='The Bot',
        policy=py_trees.common.ParallelPolicy.SuccessOnAll(synchronise=False)
    )

    # scanner
    scanner = py_trees.composites.Sequence(
        name='Scanner',
        memory=True
    )
    proc_vision = Process_vision('Proc vision')
    check_if_found  = py_trees.behaviours.CheckBlackboardVariableValue(
        name    = 'Seen carrier?',
        check   = py_trees.common.ComparisonExpression(
            variable    = 'seen_carrier',
            value       = True,
            operator    = operator.eq)
    )
    align_cmd_vel   = py_trees_ros.publishers.FromBlackboard(
        name                = 'align cmd_vel',
        topic_name          = 'cmd_vel',
        topic_type          = geometry_msgs.msg.Twist,
        qos_profile         = qos_profile_system_default,
        blackboard_variable = 'align_cmd_vel'
    )

    # obstacles
    obstacle_check = py_trees.composites.Sequence(
        name='Obstacle check',
        memory=True
    )
    proc_obstacle_check = Process_irtof('Proc irtof')
    is_obstacles = py_trees.behaviours.CheckBlackboardVariableValue(
        name='Is obstacles?',
        check=py_trees.common.ComparisonExpression(
            variable='collision',
            value=True,
            operator=operator.eq)
    )
    avoid_ob = py_trees_ros.publishers.FromBlackboard(
       name='Avoid obstacle',
       topic_name='cmd_vel',
       topic_type=geometry_msgs.msg.Twist,
       qos_profile=qos_profile_system_default,
       blackboard_variable='avoid_cmd_vel'
    )

    # random wander
    random_wander = py_trees.composites.Sequence('Wander', memory=True)
    pick_direction = Pick_random_direction('Pick direction')
    random_cmd_vel = py_trees_ros.publishers.FromBlackboard(
        name='random cmd_vel',
        topic_name='cmd_vel',
        topic_type=geometry_msgs.msg.Twist,
        qos_profile=qos_profile_system_default,
        blackboard_variable='random_cmd_vel'
    )
    wander_delay = py_trees.behaviours.TickCounter(name='Wander delay',
                                                   duration=50)

    # relationships
    root.add_child(scanner)
    scanner.add_child(proc_vision)
    scanner.add_child(check_if_found)
    scanner.add_child(align_cmd_vel)

    root.add_child(obstacle_check)
    obstacle_check.add_child(proc_obstacle_check)
    obstacle_check.add_child(is_obstacles)
    obstacle_check.add_child(avoid_ob)

    root.add_child(random_wander)
    random_wander.add_child(pick_direction)
    random_wander.add_child(random_cmd_vel)
    random_wander.add_child(wander_delay)

    return root


def main():
    rclpy.init()
    root = create_root()
    tree = py_trees_ros.trees.BehaviourTree(root=root,
                                            unicode_tree_debug=False)
    tree.setup()
    tree.tick_tock(period_ms=100.0)
    rclpy.spin(tree.node)


if __name__ == '__main__':
    main()
